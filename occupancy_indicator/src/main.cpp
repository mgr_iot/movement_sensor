#include <Homie.h>
#include <FastLED.h>

HomieNode occupancy_indicator_node("men_wc_indicator", "Men WC Indicator", "led");

const int LEDS_COUNT = 1;
const int LED_PIN = 5;
const char IS_OCCUPIED_KEY[] = "isOccupied";
uint8_t baseHue = 0;

CRGB leds[LEDS_COUNT];

void pulse();

void loopHandler() {
  pulse();
  FastLED.show();
  EVERY_N_SECONDS( 30 ) { baseHue = 0; }
  EVERY_N_SECONDS( 60 ) { baseHue = 85; }
}

void setup() {
  delay(1000);

  Serial.begin(115200);
  Homie.getLogger() << F("STARTED") << endl << endl;

  FastLED.addLeds<WS2812B, LED_PIN, GRB>(leds, LEDS_COUNT);
  FastLED.setBrightness(255);

  Homie_setFirmware("wc_indicator", "1.0.0");
  Homie_setBrand("WC_Indicator");
  Homie.setLoopFunction(loopHandler);

  occupancy_indicator_node.advertise(IS_OCCUPIED_KEY).setDatatype("boolean").setUnit("Flag").setName("Is occupied").settable();

  //Homie.setup();

}


void loop() {
  //Homie.loop();
  loopHandler();
}

void pulse() {
  uint8_t beatsPerMinute = 60;
  uint8_t beat = beatsin8( beatsPerMinute, 0, 255);
  leds[0] = CHSV(baseHue, 255, beat);
}
