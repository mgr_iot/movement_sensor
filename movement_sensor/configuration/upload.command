#! /bin/bash
# Connect to the device in comfig mode, check its IP and upload to the appropriate address
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
cd "${DIR}"
curl http://192.168.123.1/config --header "Content-Type: application/json" --upload-file "../data/homie/config.json"