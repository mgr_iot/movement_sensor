
#include <Homie.h>

const int PIR_SENSOR_PIN = D8;
const int RF_SENSOR_PIN = D2;
const char HAS_MOVEMENT_KEY[] = "hasMovement";

HomieNode pir_sensor("pir_sensor", "PIR Movement sensor", "movement");
HomieNode rf_sensor("rf_sensor", "RF Movement sensor", "movement");

bool lastPIRState = false;
bool lastRFState = false;

const char* boolToChar(bool value) {
  return value ? "true" : "false";
}

void setupHandler() {

}


void loopHandler() {
  bool currentPIRState = digitalRead(PIR_SENSOR_PIN);
  bool currentRFState = digitalRead(RF_SENSOR_PIN);
  if (currentPIRState != lastPIRState) {
    pir_sensor.setProperty(HAS_MOVEMENT_KEY).send(boolToChar(currentPIRState));
    lastPIRState = currentPIRState;
    Homie.getLogger() << F("PIR State Changed: ") << currentPIRState << endl;
  }
  if (currentRFState != lastRFState) {
    rf_sensor.setProperty(HAS_MOVEMENT_KEY).send(boolToChar(currentRFState));
    lastRFState = currentRFState;
    Homie.getLogger() << F("RF State Changed: ") << currentRFState << endl;
  }

  digitalWrite(LED_BUILTIN, !(currentPIRState || currentRFState));
}

void setup(void) {
  delay(1000);
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(PIR_SENSOR_PIN, INPUT);
  pinMode(RF_SENSOR_PIN, INPUT);

  Serial.begin(115200);
  Homie.getLogger() << F("STARTED") << endl << endl;
  
  Homie_setFirmware("wc_men_motion_sensor", "1.0.0");
  Homie_setBrand("WC_Motion_Sensor");
  Homie.setSetupFunction(setupHandler).setLoopFunction(loopHandler);

  pir_sensor.advertise(HAS_MOVEMENT_KEY).setDatatype("boolean").setUnit("Flag").setName("PIR Movement");
  rf_sensor.advertise(HAS_MOVEMENT_KEY).setDatatype("boolean").setUnit("Flag").setName("RF Movement");

  Homie.setup();
}

void loop(void) {
  Homie.loop();
}